#!/usr/bin/env bash
#SBATCH --job-name=attn50#SBATCH --output=attn50%j.log
#SBATCH --error=attn50%j.err
#SBATCH --mail-user=alhermi@uni-hildesheim.de
#SBATCH --partition=STUD
#SBATCH --gres=gpu:1

#!/usr/bin/env bash

DEVICES="0"

CUDA_VISIBLE_DEVICES="$DEVICES" python run50_attn.py



