#!/usr/bin/env bash
#SBATCH --job-name=pomo20gat
#SBATCH --output=pomo20gat%j.log
#SBATCH --error=pomo20gat%j.err
#SBATCH --mail-user=alhermi@uni-hildesheim.de
#SBATCH --partition=STUD
#SBATCH --gres=gpu:1

#!/usr/bin/env bash


DEVICES="0"

wandb login 9815f3b0e8ce08b8878d8ca9a84bca3dd70f3978
wandb offline

CUDA_VISIBLE_DEVICES="$DEVICES" python pomo20_gat.py



