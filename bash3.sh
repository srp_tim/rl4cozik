#!/usr/bin/env bash
#SBATCH --job-name=ppo50#SBATCH --output=ppo50%j.log
#SBATCH --error=ppo50%j.err
#SBATCH --mail-user=alhermi@uni-hildesheim.de
#SBATCH --partition=STUD
#SBATCH --gres=gpu:1

#!/usr/bin/env bash

DEVICES="0"

CUDA_VISIBLE_DEVICES="$DEVICES" python ppo50.py



